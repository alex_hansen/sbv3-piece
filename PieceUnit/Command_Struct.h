/*********************************************************************
 * SMARTBOARDV3
 *
 * This is the shared code for the smart board project
 *
 * Major Updates:
 * 2022-04-14
 *  - Created file
 *
 * Author - Alex Hansen
 * Date   - 2022-04-14
 *********************************************************************/

typedef struct smartboard_message {
  byte command_family;
  byte command;
  char payload[100];
};
