/*********************************************************************
 * SMARTBOARDV3
 *
 * This is the code for the SmartBoardV3 piece unit.
 *
 * Major Updates:
 * 2022-04-15
 * - Removed EasyMesh and added ESPNow
 * 2021-05-31
 * - Adjusted screen displays
 * - Added command 12 to fill the bottom half of display with a color
 * 2021-05-30
 * - Added a response to command 20 messages
 * 2021-05-17
 * - Updated to State Machine.
 * - Added a deep sleep state, triple clicking the right button. Left button to wake.
 * - Can not detect if it is on the board or not.
 *
 * Author - Alex Hansen
 * Date   - 2021-05-17
 *********************************************************************/
 
 /*********************************************************************
 * Settings
 *********************************************************************/

#define BAUD 115200
#define VERSION 20210604
#define DEBUG

/*********************************************************************
 * Device Role
 *********************************************************************/

#define ROLE_PIECE   0
#define ROLE_GRID    1
#define ROLE_CONTROL 2

#define ROLE ROLE_PIECE

//Include shared command libraray from Bridge unit
#include "Command_Struct.h"

/*********************************************************************
 * Add wifi include to get MAC Address
 *********************************************************************/
#ifdef ESP32
  #include <WiFi.h>
#else
  #include <ESP8266WiFi.h>
#endif

/*********************************************************************
 * ESPNow
 *********************************************************************/

#include <esp_now.h>

// Setup ESPNow Peer information, including generic broadcast address.
esp_now_peer_info_t peerInfo;
uint8_t broadcast_address[] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};

//ESPNow Call back prototypes:
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status);
void OnDataRecv(const uint8_t * mac, const uint8_t *incomingData, int len);

/*********************************************************************
 * Scheduler
 *********************************************************************/
 
// #define _TASK_TIMECRITICAL      // Enable monitoring scheduling overruns
// #define _TASK_SLEEP_ON_IDLE_RUN // Enable 1 ms SLEEP_IDLE powerdowns between tasks if no callback methods were invoked during the pass
// #define _TASK_STATUS_REQUEST    // Compile with support for StatusRequest functionality - triggering tasks on status change events in addition to time only
// #define _TASK_WDT_IDS           // Compile with support for wdt control points and task ids
// #define _TASK_LTS_POINTER       // Compile with support for local task storage pointer
// #define _TASK_PRIORITY          // Support for layered scheduling priority
// #define _TASK_MICRO_RES         // Support for microsecond resolution
// #define _TASK_STD_FUNCTION      // Support for std::function (ESP8266 and ESP32 ONLY)
// #define _TASK_DEBUG             // Make all methods and variables public for debug purposes
// #define _TASK_INLINE            // Make all methods "inline" - needed to support some multi-tab, multi-file implementations
// #define _TASK_TIMEOUT           // Support for overall task timeout
// #define _TASK_OO_CALLBACKS      // Support for dynamic callback method binding
#include <TaskScheduler.h>

Scheduler     userScheduler; // to control your personal task

/*********************************************************************
 * State Machines
 *********************************************************************/
byte current_state = 0;
//byte new_state = 0;

// Prototype the switch state function.
void switch_state( int new_state );

//Define the states, and any variables that the state uses:
#define STATE_STARTUP 1

#define STATE_RUNNING 2
boolean onBoard = false;
// Outgoing response variables.
#include <ArduinoJson.h>
DynamicJsonDocument onBoard_json_response(1024);

#define STATE_SHUTDOWN 3

#define STATE_NODE_MAP 4
byte state_node_map_previous_state = 0;

/*********************************************************************
 * Buttons
 *********************************************************************/

#define LEFT_BUTTON_PIN 0     //Button 0 is the Left button.
#define RIGHT_BUTTON_PIN 35   //Button 35 is the right button.

#define BUTTON_LONG_CLICK_TIME 1000
#define BUTTON_DOUBLE_CLICK_TIME 400

#include <Button2.h>

// INSTANTIATE A Button OBJECT FROM THE Bounce2 NAMESPACE
Button2 left_button(LEFT_BUTTON_PIN);
Button2 right_button(RIGHT_BUTTON_PIN);

// Default Button Prototypes
void button_pressed(Button2& btn);
void button_released(Button2& btn);
void button_changed(Button2& btn);
void button_click(Button2& btn);
void button_longClickDetected(Button2& btn);
void button_longClick(Button2& btn);
void button_doubleClick(Button2& btn);
void button_tripleClick(Button2& btn);

// User Button Prototypes
void left_button_click( Button2& btn );
void right_button_click( Button2& btn );

/*********************************************************************
 * Polling
 *********************************************************************/
#define POLL_PIN 26           // Blue Wire. This is the pin that the poll connection is made on
#define DOWN_PIN 25           // Yellow Wire. This is the pin that will monitor weither or not the piece is on a board.
boolean sendOutput = true;

/*********************************************************************
 * Battery
 *********************************************************************/
#define BATT_PIN 14           // This pin should be high, while we are measuring the battery level.
#define ADC_PIN 34

/*********************************************************************
 * TFT
 *********************************************************************/
#include <TFT_eSPI.h>       // Hardware-specific library
TFT_eSPI tft = TFT_eSPI();  // Invoke custom library
#define SCREEN_HEIGHT 240
#define SCREEN_WIDTH 135
uint32_t set_screen_color = 0x0000;

/*********************************************************************
 * Helpers
 *********************************************************************/
void send_message( String message ) {

#ifdef DEBUG
  Serial.print("Sending message \""); Serial.print( message ); Serial.println("\"");
#endif

  smartboard_message message_to_send;
  message_to_send.command_family = 1;
  message_to_send.command = 2;
  message_to_send.payload[0] = 0;

  esp_err_t result = esp_now_send(broadcast_address, (uint8_t *) &message_to_send, sizeof(message_to_send));
   
  if (result == ESP_OK) {
    Serial.println("Sent with success");
  }
  else {
    Serial.print("Error sending the data: "); Serial.println(result);
  }
  //mesh.sendBroadcast( message );  
  //mesh.sendSingle( CONTROL_UNIT_ADDR, message);
}

void toggle_output( void ) {
  sendOutput = !sendOutput;

  // String message = "{ Polling:\"";
  // message += String(sendOutput);
  // message += "\" }";

  // send_message( message );   
}

void set_onBoard( bool newOnBoard ) {
  onBoard = newOnBoard;
  String response_message = "";

  onBoard_json_response["command"] = 4;    
  if ( onBoard == true )
    onBoard_json_response["command"] = 8;

  onBoard_json_response["pin"] = onBoard;
  serializeJson(onBoard_json_response, response_message);
  send_message( response_message );   
}

/*********************************************************************
 * ESP Now Call Backs
 *********************************************************************/

// callback when data is sent
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {
  char macStr[18];
  Serial.print("Packet to: ");
  // Copies the sender mac address to a string
  snprintf(macStr, sizeof(macStr), "%02x:%02x:%02x:%02x:%02x:%02x",
           mac_addr[0], mac_addr[1], mac_addr[2], mac_addr[3], mac_addr[4], mac_addr[5]);
  Serial.print(macStr);
  Serial.print(" send status:\t");
  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
}

// callback function that will be executed when data is received
void OnDataRecv(const uint8_t * mac_addr, const uint8_t *incomingData, int len) {
#ifdef DEBUG   
  char macStr[18];

  // Copies the sender mac address to a string
  snprintf(macStr, sizeof(macStr), "%02x:%02x:%02x:%02x:%02x:%02x",
           mac_addr[0], mac_addr[1], mac_addr[2], mac_addr[3], mac_addr[4], mac_addr[5]);
  
  Serial.print("Received: "); Serial.print(len); Serial.print(" from "); Serial.println(macStr);;
#endif  
}

/*********************************************************************
 * User Tasks
 *********************************************************************/
void userFunction( void ) {
  toggle_output(); 
}
Task userFunctionTask( TASK_SECOND * 1, TASK_FOREVER, &userFunction );

void check_for_onBoard_function(void ) {
  //Checks to see if the piece in on the board by looking for a high singnal on DOWN_PIN

  boolean reading = digitalRead( DOWN_PIN );

  if ( onBoard != reading )
    set_onBoard( reading );
}
Task check_for_onBoard_task( TASK_SECOND * 0.1, TASK_FOREVER, &check_for_onBoard_function );

void battery_status_Function( void ) {
  tft.setCursor (0, 38);  
  digitalWrite( BATT_PIN, HIGH );
  uint16_t v = analogRead(ADC_PIN);
  float battery_voltage = ((float)v / 4095.0) * 7.26;
  String voltage = "Voltage: " + String(battery_voltage) + "V";
  tft.println( voltage );
  digitalWrite( BATT_PIN, LOW );
}
Task batteryStatusFunctionTask( TASK_SECOND * 1, TASK_FOREVER, &battery_status_Function );

void update_screen_function( void ) {
  // If we are sending output, change the screen to blue.

  //  fillRect(0, 0, _width, _height, color);
  // +------+------+
  // | Poll | Down |
  // +------+------+
  // |  set color  |
  // +-------------+

  if ( sendOutput == true )
    tft.fillRect(0, 0, 68, 120, TFT_BLUE );
  else
    tft.fillRect(0, 0, 68, 120, TFT_BLACK );

  if ( onBoard == true )
    tft.fillRect(69, 0, 68, 120, TFT_RED );
  else
    tft.fillRect(69, 0, 68, 120, TFT_BLACK );

  tft.fillRect(0, 121, 135, 120, set_screen_color );


  // if ( ( sendOutput == true ) && ( onBoard == false ) )
  //   tft.fillScreen(TFT_GREEN);
  // else if ( ( sendOutput == true ) && ( onBoard == true ) )
  //   tft.fillScreen(TFT_BLUE);
  // else if ( ( sendOutput == false ) && ( onBoard == true ) )
  //   tft.fillScreen(TFT_RED);
  // else 
  //   tft.fillScreen(TFT_BLACK);  
}
Task updateScreenTask( TASK_SECOND * 0.25, TASK_FOREVER, &update_screen_function );

void print_node_map_function ( void ) {
  tft.fillScreen(TFT_BLACK);
  tft.setCursor (0, 38);


  tft.println("Connection list:");
  tft.println();  
}
Task print_node_map_task( TASK_SECOND * 0.1, TASK_FOREVER, &print_node_map_function );

/*********************************************************************
 * Setups
 *********************************************************************/
void ESPNow_Setup() {
  WiFi.mode(WIFI_MODE_STA);

#ifdef DEBUG  
  Serial.print("MAC Address is: "); Serial.println(WiFi.macAddress());
#endif

  WiFi.disconnect();

  // Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  // Once ESPNow is successfully Init, we will register for Send CB to
  // get the status of Trasnmitted packet
  esp_now_register_send_cb(OnDataSent);

  // Once ESPNow is successfully Init, we will register for Recv CB to
  // get recv packer info
  esp_now_register_recv_cb(OnDataRecv);  

  //Register peer
  memcpy(peerInfo.peer_addr, broadcast_address, 6);
  peerInfo.channel = 0;
  peerInfo.encrypt = false;

  if (esp_now_add_peer(&peerInfo) != ESP_OK) {
      Serial.println("Failed to add peer");
      return;
  }  
}

void button_setup() { 
  // left_button.setLongClickTime(1000);
  // left_button.setDoubleClickTime(400);

  // left_button.setChangedHandler(button_changed);
  // left_button.setPressedHandler(button_pressed);
  // left_button.setReleasedHandler(button_released);
  // left_button.setTapHandler(tap);
  left_button.setClickHandler(left_button_click);
  left_button.setLongClickDetectedHandler(button_longClickDetected);
  left_button.setLongClickHandler(button_longClick);
  left_button.setDoubleClickHandler(left_button_doubleClick);
  left_button.setTripleClickHandler(left_button_triple_click);

  // right_button.setLongClickTime(1000);
  // right_button.setDoubleClickTime(400);

  // right_button.setChangedHandler(button_changed);
  // right_button.setPressedHandler(button_pressed);
  // right_button.setReleasedHandler(button_released);
  // right_button.setTapHandler(tap);
  right_button.setClickHandler(right_button_click);
  right_button.setLongClickDetectedHandler(button_longClickDetected);
  right_button.setLongClickHandler(button_longClick);
  right_button.setDoubleClickHandler(right_button_doubleClick);
  right_button.setTripleClickHandler(right_button_triple_click);
}

void task_setup() {
  userScheduler.addTask( userFunctionTask );
  userScheduler.addTask( updateScreenTask );
  userScheduler.addTask( batteryStatusFunctionTask );
  userScheduler.addTask( check_for_onBoard_task );
  userScheduler.addTask( print_node_map_task );
}

void setup() {

  // Serial setup
  Serial.begin( BAUD );

  // Set output pins
  pinMode( POLL_PIN, OUTPUT );
  pinMode( DOWN_PIN, INPUT_PULLDOWN );
  pinMode( BATT_PIN, OUTPUT );
  digitalWrite( BATT_PIN, LOW );

  // Setup the TFT Screen and fill it with black
  tft.init();
  tft.fillScreen(TFT_BLACK); 

  // ESPNow Setup
  ESPNow_Setup();
  
  // Task Setup
  task_setup();

  // Button Setup
  button_setup();

  // Set our inital state:
  switch_state( STATE_STARTUP );
}

/*********************************************************************
 * Loop
 *********************************************************************/
void loop() {
  userScheduler.execute();
  left_button.loop();
  right_button.loop();

  //These actions must always be completed.
  digitalWrite(POLL_PIN,sendOutput);  // Adjust the output pin to the value of "sendOutput"
}

/*********************************************************************
 * STATE MACHINE
 *********************************************************************/
void switch_state( int new_state ) {
  /*
  Switch from one state to another. This will short circuit is the state doesn't really change.
  */

  //Check to see if we've changed states, and if not return
  if ( current_state == new_state ) return;

  Serial.print("Changing state from "); Serial.print( current_state ); Serial.print(" to "); Serial.println( new_state );
  
  //Stop all tasks, the state list below should enable them as needed.
  //userScheduler.disableAll();
  userFunctionTask.disable();
  updateScreenTask.disable();
  batteryStatusFunctionTask.disable();
  check_for_onBoard_task.disable();
  print_node_map_task.disable();  

  switch ( new_state ) {
    case STATE_STARTUP : { switch_state( STATE_RUNNING ); break; }
    case STATE_RUNNING : { check_for_onBoard_task.enable(); userFunctionTask.enable(); updateScreenTask.enable(); batteryStatusFunctionTask.enable(); break; }
    case STATE_SHUTDOWN : { esp_sleep_enable_ext0_wakeup(GPIO_NUM_0,0); esp_deep_sleep_start(); break;}
    case STATE_NODE_MAP : { print_node_map_task.enable(); break; }
  }

  // Now update our current state to the new state
  current_state = new_state;
}

/*********************************************************************
 * Command Responses
 *********************************************************************/
void do_send_details( uint32_t from ){
  Serial.println("In do_send_details");

  // Outgoing response variables.
  DynamicJsonDocument json_response(1024);
  String response_message = "";

  json_response["command"] = 21;
  json_response["Type"] = ROLE;
  json_response["Version"] = VERSION;

  serializeJson(json_response, response_message);
  Serial.println(response_message);
  send_message( response_message );
}

void do_set_polling_pin( uint32_t from, bool pin_value ){
  Serial.println("In do_set_polling_pin_high");

  // Outgoing response variables.
  DynamicJsonDocument json_response(1024);
  String response_message = "";

  if ( onBoard == false )
    json_response["command"] = 4;
  else {
    sendOutput = pin_value;    
    json_response["command"] = 2;
    json_response["pin"] = sendOutput;
  }

  serializeJson(json_response, response_message);
  send_message( response_message );
}

void do_set_screen_color( uint32_t from, uint32_t color ){
  Serial.println("In do_set_screen_color");

  // Outgoing response variables.
  DynamicJsonDocument json_response(1024);
  String response_message = "";

  Serial.print("Color is"); Serial.println( color );

  set_screen_color = color;

  json_response["command"] = 2;

  serializeJson(json_response, response_message);
  send_message( response_message );
}

/*********************************************************************
 * User Button Call backs - Prototypes above.
 *********************************************************************/
void left_button_click( Button2& btn ) {
  toggle_output();
//  Serial.println("Toggled output:");
//  Serial.println( sendOutput );
}

void left_button_doubleClick(Button2& btn) {
  set_onBoard( !onBoard );
  Serial.print("OnBoard is "); Serial.println(onBoard);  
}

void right_button_doubleClick(Button2& btn) {
  if ( check_for_onBoard_task.isEnabled() == true )
    check_for_onBoard_task.disable();
  else
    check_for_onBoard_task.enable();
  Serial.print("OnBoard task is enabled? "); Serial.println(check_for_onBoard_task.isEnabled());
}


void right_button_click( Button2& btn ) {
  if ( userFunctionTask.isEnabled() == true )
    userFunctionTask.disable();
   else
    userFunctionTask.enable();
//   Serial.println("Toggles Task");
//   Serial.println( userFunctionTask.isEnabled()  );
}

void right_button_triple_click( Button2& btn) {
  switch_state( STATE_SHUTDOWN );
}

void left_button_triple_click( Button2& btn ) {
  if ( current_state != STATE_NODE_MAP ) {
    state_node_map_previous_state = current_state;
    switch_state( STATE_NODE_MAP );
  }
  else {
    switch_state( state_node_map_previous_state );
  }
}

/*********************************************************************
 * Button Call backs - Prototypes above.
 *********************************************************************/
void button_pressed(Button2& btn) {
    Serial.println("pressed");  
}
void button_released(Button2& btn) {
    Serial.print("released: ");
    Serial.println(btn.wasPressedFor());  
}
void button_changed(Button2& btn) {
    Serial.println("changed");
}
void button_click(Button2& btn) {
    Serial.println("click\n");
}
void button_longClickDetected(Button2& btn) {
    Serial.println("long click detected\n");
}
void button_longClick(Button2& btn) {
    Serial.println("long click\n");
}
void button_doubleClick(Button2& btn) {
    Serial.println("double click\n");
}
void button_tripleClick(Button2& btn) {
    Serial.println("triple click\n");
}
void button_tap(Button2& btn) {
    Serial.println("tap");
}
